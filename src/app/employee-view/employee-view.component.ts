import { Component, OnInit, HostListener } from '@angular/core';

@Component({
  selector: 'app-employee-view',
  templateUrl: './employee-view.component.html',
  styleUrls: ['./employee-view.component.css']
})
export class EmployeeViewComponent implements OnInit {

  employees = [];

  constructor() { }

  ngOnInit(): void {
    let entries = Object.entries(localStorage);
    for (let entry of entries) {
      this.employees.push(JSON.parse(localStorage.getItem(entry[0])));
    }
  }

  removeEmployee(eId: string): void {
    console.log(eId);
    localStorage.removeItem(eId.toString());
    window.location.reload();
  }
}
