import { formatDate } from '@angular/common';
import { Component, OnInit, HostListener, Host } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-employee-details',
  templateUrl: './employee-details.component.html',
  styleUrls: ['./employee-details.component.css']
})
export class EmployeeDetailsComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
  
  employeeForm = new FormGroup({
    eId: new FormControl(''),
    name: new FormControl(''),
    designation: new FormControl(''),
    about: new FormControl(''),
    doj: new FormControl(formatDate(new Date().toDateString(), 'yyyy/MM/dd', 'en')),
  })

  eId: string = '';

  onSubmit() {
    const empDetails = this.employeeForm.value;
    this.eId = this.employeeForm.controls.eId.value;
    if(localStorage.getItem(this.eId)) {
      alert('Kindly enter a different employee id');
    } else {
      localStorage.setItem(this.eId, JSON.stringify(empDetails));
      this.employeeForm.reset();
      alert('User data entered');
    }
  }

  getValues() {
    for (let key in localStorage) {
      console.log(localStorage.getItem(key));
    }
  }

}
