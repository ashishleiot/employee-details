import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EmployeeDetailsComponent } from './employee-details/employee-details.component';
import { EmployeeViewComponent } from './employee-view/employee-view.component';

const routes: Routes = [
  { path: '', redirectTo: '/registration', pathMatch: "full" },
  { path: 'registration', component: EmployeeDetailsComponent },
  { path: 'views', component: EmployeeViewComponent },
  { path: '**', redirectTo: '/registration' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
